# Building a Second Brain with [Hugo](https://gohugo.io)

> Objective: create a search-able wiki for markdown files with Hugo.

> Problem: render collection of markdown files with no front matter or date in the file name.

#Steps: 
0. Create sample content:
	Sample content that will be used in the next steps.
	
1. Most basic setup for markdown files with no frontmatter with Hugo
	Use relative urls to make wiki portable. Based on the discussion at
	[Gohugo without baseurl ? or with relative url only? - support - HUGO](https://discourse.gohugo.io/t/gohugo-without-baseurl-or-with-relative-url-only/2779/2)
	
	Make following chnages to config:
	baseurl = ""
	relativeurls = true
	uglyurls = true

	
2. Get post name from file name
	Based on the discussion at
	[dennislee.xyz/2020-06-20-hugo-change-the-automatic-title-and-slug.md at master · dennisleexyz/dennislee.xyz · GitHub](https://github.com/dennisleexyz/dennislee.xyz/blob/master/content/blog/2020-06-20-hugo-change-the-automatic-title-and-slug.md)
		
3. Order posts by file name
	Based on the discussion at
	[Sort by Computed Field in Hugo – Tom Hummel](https://tomhummel.com/2018/10/04/hugo-sort-by-computed-field/)
	
4. Open external urls in the new window
	Based on the discussion at
	* [How to Open Link in New Tab with Hugo's new Goldmark Markdown Renderer | Prasad's Pen](https://agrimprasad.com/post/hugo-goldmark-markdown/#:~:text=Thus%2C%20if%20you%20want%20to,_blank%22%20attribute%20to%20the%20links.)
		
5. Add minimal style
	Based on the [minimal](https://github.com/calintat/minimal) theme.
			
6. Add search
	Based on the discussion at
	[Hugo JS Searching with Fuse.js · GitHub](https://gist.github.com/eddiewebb/735feb48f50f0ddd65ae5606a1cb41ae#sample)

7. Add search cache
	Speedup search by loading json search index as javascript file.
	
8. Search overlay
	Present search as an overlay window instead of loading new page.
	
9. Custom search
	Use custom search instead of Fuse.js to speedup search
	
10.	Add dark theme
	Based on the discussion at
	[Adding dark mode to a Hugo static website without learning CSS | radu's blog](https://radu-matei.com/blog/dark-mode/)

11. Add PWA
	Add PWA for off-line functionality.
	
	Based on the [PWABuilder](https://www.pwabuilder.com/)
	
12. Improve search speed	
	Improve search speed by removing common words. I.e. remove 'and', 'not', if', 'the', 'from', 'to', etc

13. Text to speech
	Based on the [How to Add Text-to-Speech Feature on Any Web Page - Hongkiat](https://www.hongkiat.com/blog/text-to-speech/)
	Right click mouse event based on [javascript - Capture Right Click](https://stackoverflow.com/questions/1093065/capture-right-click-on-html-div)
	
	Clean up navigation.
	

# Ideas for future:
- Add functionality to create world clouds. The world cloud functionality can be based on [GitHub - psykhi/wordclouds: Wordclouds in go](https://github.com/psykhi/wordclouds) project.
- Add linkage graph to visualize similar posts. The connections can be based on [Text Similarities : Estimate the degree of similarity between two texts | by Adrien Sieg | Medium](https://medium.com/@adriensieg/text-similarities-da019229c894) and
	visualized with [Zoomable Force Directed Graph d3v4 - bl.ocks.org](https://bl.ocks.org/puzzler10/4438752bb93f45dc5ad5214efaa12e4a)

- Implement with [Zola](https://www.getzola.org/documentation/getting-started/cli-usage/). Not currently possible due to [Make the page front-matter section optional · Issue #374 · getzola/zola · GitHub](https://github.com/getzola/zola/issues/374)
	

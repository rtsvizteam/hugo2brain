// Get the modal
var modal = document.getElementById("searchModal");
var searchInput = document.getElementById("search-query");



function toggleSearchDialog() {
	//alert('search');	
	modal.classList.toggle("open");
	searchInput.focus();
}

document.addEventListener('keydown', function(e) {
	if (e.which == 27) {
		e.preventDefault();
		toggleSearchDialog()
	} else if (e.which == 191 && e.shiftKey == false) {
		e.preventDefault();
		toggleSearchDialog()
	}
}, false);


	
function run_search(ele) {
	// either only activate on enter or on any key press
    if(event.key === 'Enter') {
        //alert(ele.value);        
		doSearch(ele.value);        
    }
}



summaryInclude=60;
var fuseOptions = {
  shouldSort: true,
  includeMatches: true,
  threshold: 0.1, // for parsing diacritics
  tokenize:true,
  location: 0,
  distance: 100,
  maxPatternLength: 32,
  minMatchCharLength: 3,
  keys: [
    {name:"title",weight:0.8},
    {name:"content",weight:0.5}
  ]
};


function doSearch(searchQuery) {
  if (searchQuery) {
	var e = document.getElementById("search-results")
	var child = e.lastElementChild;  
    while (child) { 
		e.removeChild(child); 
        child = e.lastElementChild; 
    } 	
		
    executeSearch(searchQuery);
  }
}


function executeSearch(searchQuery) {
	var timingStore = (new Date()).toLocaleString();
	  	  
    var pages = searchData;
    var fuse = new Fuse(pages, fuseOptions);
    var result = fuse.search(searchQuery);
    console.log({
      "matches": result
    });
    if (result.length > 0) {
      populateResults(result, searchQuery);
    } else {
      var para = document.createElement("P");
      para.innerText = "No matches found";
      document.getElementById("search-results").appendChild(para);
    }
	
    var change = (((new Date()) - Date.parse(timingStore)))
    console.log("Search in " + change +" ms");	
}


function populateResults(result, searchQuery) {
  result.forEach(function (value, key) {
    var content = value.item.content;
    var snippet = "";
    var snippetHighlights = [];
    var tags = [];
    if (fuseOptions.tokenize) {
      snippetHighlights.push(searchQuery);
    } else {
      value.matches.forEach(function (mvalue, matchKey) {
        if (mvalue.key == "tags" || mvalue.key == "categories") {
          snippetHighlights.push(mvalue.value);
        } else if (mvalue.key == "content") {
          start = mvalue.indices[0][0] - summaryInclude > 0 ? mvalue.indices[0][0] - summaryInclude : 0;
          end = mvalue.indices[0][1] + summaryInclude < content.length ? mvalue.indices[0][1] + summaryInclude : content.length;
          snippet += content.substring(start, end);
          snippetHighlights.push(mvalue.value.substring(mvalue.indices[0][0], mvalue.indices[0][1] - mvalue.indices[0][0] + 1));
        }
      });
    }

    if (snippet.length < 1) {
      snippet += content.substring(0, summaryInclude * 2);
    }
    //pull template from hugo templarte definition
    var templateDefinition = document.getElementById("search-result-template").innerHTML;
    //replace values
    var output = render(templateDefinition, {
      key: key,
      title: value.item.title,
      link: value.item.permalink,
      tags: value.item.tags,
      categories: value.item.categories,
      snippet: snippet
    });
	
    document.getElementById("search-results").appendChild(htmlToElement(output));
	
    snippetHighlights.forEach(function (snipvalue, snipkey) {
      new Mark(document.getElementById("summary-" + key)).mark(snipvalue);
    });

  });
}

function render(templateString, data) {
  var conditionalMatches, conditionalPattern, copy;
  conditionalPattern = /\$\{\s*isset ([a-zA-Z]*) \s*\}(.*)\$\{\s*end\s*}/g;
  //since loop below depends on re.lastInxdex, we use a copy to capture any manipulations whilst inside the loop
  copy = templateString;
  while ((conditionalMatches = conditionalPattern.exec(templateString)) !== null) {
    if (data[conditionalMatches[1]]) {
      //valid key, remove conditionals, leave content.
      copy = copy.replace(conditionalMatches[0], conditionalMatches[2]);
    } else {
      //not valid, remove entire section
      copy = copy.replace(conditionalMatches[0], '');
    }
  }
  templateString = copy;
  //now any conditionals removed we can do simple substitution
  var key, find, re;
  for (key in data) {
    find = '\\$\\{\\s*' + key + '\\s*\\}';
    re = new RegExp(find, 'g');
    templateString = templateString.replace(re, data[key]);
  }
  return templateString;
}

/**
 * By Mark Amery: https://stackoverflow.com/a/35385518
 * @param {String} HTML representing a single element
 * @return {Element}
 */
function htmlToElement(html) {
    var template = document.createElement('template');
    html = html.trim(); // Never return a text node of whitespace as the result
    template.innerHTML = html;
    return template.content.firstChild;
}

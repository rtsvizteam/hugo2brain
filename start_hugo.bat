::--------------------------------------------
:: Setup
::--------------------------------------------

SET execute=build
SET folder=01.basic
SET folder=02.basic-name
SET folder=03.basic-sort-name
SET folder=04.basic-url-new-window
SET folder=05.add-style

SET execute=serve
SET folder=06.add-search

SET execute=build
SET folder=07.add-search-cache
SET folder=08.overlay-search
SET folder=09.custom-search
SET folder=10.dark-theme
SET folder=11.pwa
SET folder=12.search-speed


::--------------------------------------------
:: Clean up
::--------------------------------------------
SET startfolder=%~dp0

cd %folder%
rm -f -r public
rm -f -r resources

rm -f -r content 
mkdir content
cp -r ../00.sample.content/* content

::--------------------------------------------
:: Build
::--------------------------------------------

IF %execute% == build  (
	:: disable sitemap and tags,categories
	hugo --disableKinds=sitemap,taxonomyTerm
	start chrome %startfolder%%folder%\public\index.html 	
) ELSE (
	start chrome http://localhost:8080
	hugo server -p8080 --disableKinds=sitemap,taxonomyTerm
)

::--------------------------------------------
:: Go back
::--------------------------------------------
cd ..





// Dark theme
// source: radu-matei.com/blog/dark-mode
var toggleDark = document.getElementById("dark-mode-toggle");
var darkTheme = document.getElementById("dark-mode-theme");


// the default theme is light
//* [Why using localStorage directly is a bad idea - Michal Zalecki](https://michalzalecki.com/why-using-localStorage-directly-is-a-bad-idea/)
// use url to store values like in original search

try { 
var savedTheme = localStorage.getItem("dark-mode-storage") || (darkTheme.disabled ? "light" : "dark");
setTheme(savedTheme);
 } catch (e) {}

function setTheme(mode) {
try { localStorage.setItem("dark-mode-storage", mode); } catch (e) {}  

  if (mode === "dark") {
    darkTheme.disabled = false;
  } else if (mode === "light") {
    darkTheme.disabled = true;
  }
} 
	
function toggleTheme() {
  if (darkTheme.disabled) 
	  setTheme("dark");
  else 
	  setTheme("light");
} 


// Search
// source: radu-matei.com/blog/dark-mode
const modal = document.getElementById("searchModal");
const searchInput = document.getElementById("search-query");
var searchData = JSON.parse(searchData);

// Focus
const main_block = document.getElementsByTagName("main")[0];
const search_results = document.getElementById('search-results');
main_block.focus();




function toggleSearchDialog() {
	//alert('search');	
	modal.classList.toggle("open");
	if(modal.classList.length > 1)
		searchInput.focus();
	else
		main_block.focus();
		
}

function scrollUp() {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
	
	main_block.scrollTop = 0;
	search_results.scrollTop = 0;
}

// www.cambiaresearch.com/articles/15/javascript-char-codes-key-codes
document.addEventListener('keydown', function(e) {
	if (e.which == 27) {
		e.preventDefault();
		toggleSearchDialog();
	} else if (e.which == 191 && e.shiftKey == false) {
		e.preventDefault();
		toggleSearchDialog();
		
	} else if (e.ctrlKey && e.which == 68) { // ctrl+d - chnage theme
		e.preventDefault();
		toggleTheme();
	} else if (e.ctrlKey && e.which == 80) { // ctrl+p - print
		e.preventDefault();
		window.print(); 
	} else if (e.ctrlKey && e.which == 85) { // ctrl+u - scroll up
		e.preventDefault();
		scrollUp(); 
	} else if (e.ctrlKey && e.which == 90) { // ctrl+z - play / pause
		e.preventDefault();
		toggleSpeak(0); 	
	} else if (e.ctrlKey && e.which == 88) { // ctrl+x - reset play
		e.preventDefault();
		toggleSpeak(1); 
	}	

	
}, false);


	
function run_search(ele) {
	if (event.key === 'Enter' || (event.which <= 90 && event.which >= 48) || 
		event.which==13 || event.which==8 || event.which==46 || event.which>= 186) {
		doSearch(ele.value);        
    }
}


// number of char to include into summary
summaryInclude=60;


function doSearch(searchQuery) {
  if (searchQuery) {
	  
	// clean up element
	const e = document.getElementById("search-results");
	while(e.firstChild) {
		e.removeChild(e.firstChild);
	}	
		
    executeSearch(searchQuery);
  }
}



function search2(text){
  const terms = searchQuery.trim().replace(/ /g, '|')  
  regexpr = new RegExp(terms, "im"); // ignore case
  const filter = matchAll(regexpr);  
  return searchData.filter(filter);
}
function matchAll2(regexpr){
  return function (json) {
 	  return json.content.search(regexpr) >= 0 ? true : false;
  }
}


function search(text){	
    //Replace regex reserved characters
    text=text.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    //Split your string at spaces
    arrWords = text.split(" ");
    //Encapsulate your words inside regex groups
    arrWords = arrWords.map(function( n ) {
        return ["(?=.*"+n+")"];
    });
    //Create a regex pattern
    regexpr = new RegExp("^"+arrWords.join("")+".*$","im");
	
  const filter = matchAll(regexpr);  
  return searchData.filter(filter);
}

function matchAll(regexpr){
  return function (json) {
 	  return json.content.search(regexpr) >= 0 ? true : false;
  }
}


//var searchTime=0.0
//var renderTime=0.0
//var times = 0.0;

function executeSearch(searchQuery) {
	var timingStore = (new Date()).toLocaleString();
	  	  
	
	result = search(searchQuery);
	//console.log({"matches": result});
	
	//times = times + 1
	//searchTime = searchTime + (((new Date()) - Date.parse(timingStore)));	
	//console.log("Search in " + searchTime/times +" ms");	
		
	//timingStore = (new Date()).toLocaleString();		 
	
    
    if (result.length > 0) {
      populateResults(result, searchQuery);
    } else {
      var para = document.createElement("P");
      para.innerText = "No matches found";
      document.getElementById("search-results").appendChild(para);
    }
	
	//renderTime = renderTime + (((new Date()) - Date.parse(timingStore)));	
    //console.log("Render in " + renderTime/times +" ms");	
}


function populateResults(result, searchQuery) {
	// output
	//const dom = document.createDocumentFragment();
	//const searchList = [];
	
	result.forEach((value) => {
		var content = value.content;
		var snippet = "";
		snippet += content.substring(0, summaryInclude * 2);
		
		// highlight
		regexpr = new RegExp(searchQuery.trim().replace(/ /g, '|'), "img"); // ignore case
		snippet = snippet.replace(regexpr, str => "<span class='hl'>" + str + "</span>")
		
		const topItem = document.createElement("div");	
		topItem.insertAdjacentHTML(
			"beforeend",
			`<h4><a href="${baseURL}${value.permalink}">${value.title}</a></h4>` + `<p>${snippet}</p>`
		);

		document.getElementById("search-results").appendChild(topItem);
		//searchList.push(topItem);
	});
	
	//dom.append(...searchList);
	//document.getElementById("search-results").appendChild(dom);
}



// Zoom
Array.prototype.slice.call(document.getElementsByTagName("img")).forEach(function(value, index) {
	value.onclick = function zoom(e){
	  var zoomer = e.currentTarget;
	  zoomer.classList.toggle("zoom");
	}
})

//PWA
if ('serviceWorker' in navigator) {
    navigator.serviceWorker
		.register(baseURL+'index.js', { scope: './' })
        .then(() => {
            console.info('Worker Registered');
        }, err => console.error("Worker registration failed: ", err));

    navigator.serviceWorker
        .ready
        .then(() => {
            console.info('Service Worker Ready');
        });
}


//Text to Speech
window.speechSynthesis.cancel(); // cancel if already playing
//it takes time to get voice and create speech object
var voices = window.speechSynthesis.getVoices(); // get Voices
function getSSText() {
	var $text = document.querySelector('main').cloneNode(true);
	Array.prototype.forEach.call( $text.querySelectorAll('p'), function( $el ){
		$el.innerHTML = $el.textContent.replace(/(\r\n|\n|\r)/gm,' ').replace(/(\t| )+/gm,' ').trim() + '\n'
	});
	$text = $text.textContent.replace(/(\t| )+/gm,' ').replace(/(\r\n|\n|\r){2,}/gm,'\n\n').trim()
	return $text;
	
}
var ss = new SpeechSynthesisUtterance(getSSText());
ss.voice = voices[0];				

// selection text - only desktop
function getSelectionText() {
    var text = "";
    if (window.getSelection) {
        text = window.getSelection().toString();
    } else if (document.selection && document.selection.type != "Control") {
        text = document.selection.createRange().text;
    }
    return text;
}

var speakFlag = false;
var pauseFlag = false;

function toggleSpeak(mode) {		
		if (!speakFlag) {
			speakFlag = true; pauseFlag = false;
			
			var ss0 = ss;
			var $text = getSelectionText()
			if($text.length > 0) {
				$text = $text.replace(/(\r\n|\n|\r)/gm,' ').trim()			
				ss0 = new SpeechSynthesisUtterance($text);
				ss0.voice = voices[0];
			}
			
			ss0.addEventListener('end', function(){
				pauseFlag = speakFlag = false;
			});
			
			window.speechSynthesis.cancel();
			window.speechSynthesis.speak(ss0);	
		} else {
			if (mode == 0) {
			
				if (pauseFlag) { 
					if(window.speechSynthesis.paused) {
						window.speechSynthesis.resume(); 
						pauseFlag = !pauseFlag;	
					} else {
						// something is wrong: cancel and restart
						speakFlag = true; pauseFlag = false;
						window.speechSynthesis.cancel(); 
						window.speechSynthesis.speak(ss);	
					}
				}
				else { 
					window.speechSynthesis.pause(); 
					pauseFlag = !pauseFlag;				
				}								
			} else {
				window.speechSynthesis.cancel();				
				pauseFlag = speakFlag = false;	
			}
		}		
} 

		

# Recipe Air

* 1 pound extra-firm tofu, drained and patted dry
* 2 large eggs
* 1 tablespoon Dijon mustard
* 1 teaspoon ground cumin
* 1/4 teaspoon fine grain sea salt
* 1 tablespoon extra virgin olive oil

[Source](http://www.101cookbooks.com/)
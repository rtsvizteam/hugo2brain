# Recipe Water

* 3 handfuls of ripe tomatoes
* A small handful of basil leaves
* 4 tablespoons extra-virgin olive oil
* 2 cloves garlic, peeled

[Source](http://www.101cookbooks.com/)
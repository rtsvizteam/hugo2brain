// Get the modal
const modal = document.getElementById("searchModal");
const searchInput = document.getElementById("search-query");


function toggleSearchDialog() {
	//alert('search');	
	modal.classList.toggle("open");
	searchInput.focus();
}

document.addEventListener('keydown', function(e) {
	if (e.which == 27) {
		e.preventDefault();
		toggleSearchDialog()
	} else if (e.which == 191 && e.shiftKey == false) {
		e.preventDefault();
		toggleSearchDialog()
	}
}, false);


	
function run_search(ele) {
	if (event.key === 'Enter' || (event.which <= 90 && event.which >= 48) || 
		event.which==13 || event.which==8 || event.which==46 || event.which>= 186) {
		doSearch(ele.value);        
    }
}



summaryInclude=60;


function doSearch(searchQuery) {
  if (searchQuery) {
	  
	// clean up element
	const e = document.getElementById("search-results");
	while(e.firstChild) {
		e.removeChild(e.firstChild);
	}	
		
    executeSearch(searchQuery);
  }
}



function search2(text){
  const terms = searchQuery.trim().replace(/ /g, '|')  
  regexpr = new RegExp(terms, "im"); // ignore case
  const filter = matchAll(regexpr);  
  return searchData.filter(filter);
}
function matchAll2(regexpr){
  return function (json) {
 	  return json.content.search(regexpr) >= 0 ? true : false;
  }
}


function search(text){	
    //Replace regex reserved characters
    text=text.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
    //Split your string at spaces
    arrWords = text.split(" ");
    //Encapsulate your words inside regex groups
    arrWords = arrWords.map(function( n ) {
        return ["(?=.*"+n+")"];
    });
    //Create a regex pattern
    regexpr = new RegExp("^"+arrWords.join("")+".*$","im");
	
  const filter = matchAll(regexpr);  
  return searchData.filter(filter);
}

function matchAll(regexpr){
  return function (json) {
 	  return json.content.search(regexpr) >= 0 ? true : false;
  }
}


var searchTime=0.0
var renderTime=0.0
var times = 0.0;

function executeSearch(searchQuery) {
	var timingStore = (new Date()).toLocaleString();
	  	  
	
	result = search(searchQuery);
	//console.log({"matches": result});
	
	times = times + 1
	searchTime = searchTime + (((new Date()) - Date.parse(timingStore)));	
	console.log("Search in " + searchTime/times +" ms");	
	
	
	timingStore = (new Date()).toLocaleString();		 
	
    
    if (result.length > 0) {
      populateResults(result, searchQuery);
    } else {
      var para = document.createElement("P");
      para.innerText = "No matches found";
      document.getElementById("search-results").appendChild(para);
    }
	
	renderTime = renderTime + (((new Date()) - Date.parse(timingStore)));	
    console.log("Render in " + renderTime/times +" ms");	
}


function populateResults(result, searchQuery) {
	// output
	//const dom = document.createDocumentFragment();
	//const searchList = [];
	
	result.forEach((value) => {
		var content = value.content;
		var snippet = "";
		snippet += content.substring(0, summaryInclude * 2);
		
		// highlight
		regexpr = new RegExp(searchQuery.trim().replace(/ /g, '|'), "img"); // ignore case
		snippet = snippet.replace(regexpr, str => "<span class='hl'>" + str + "</span>")
		
		const topItem = document.createElement("div");	
		topItem.insertAdjacentHTML(
			"beforeend",
			`<h4><a href="${baseURL}${value.permalink}">${value.title}</a></h4>` + `<p>${snippet}</p>`
		);

		document.getElementById("search-results").appendChild(topItem);
		//searchList.push(topItem);
	});
	
	//dom.append(...searchList);
	//document.getElementById("search-results").appendChild(dom);
}




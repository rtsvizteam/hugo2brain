const version = {{ .Site.Params.cache_name | default "1.00" }};
const cacheName = `content-v${version}`;


const precache = [
    // page cache
    './',
	'./index.json',
	'./index.html',
	'./index.js',
{{- /* Generate all files in static folder. */ -}}
{{- $rootFolder := "static" -}}
{{- $nskip := len $rootFolder  -}} {{- $buffer := newScratch -}} {{- $buffer.Add "new" (slice $rootFolder) -}}
{{- range (seq 10) -}} {{- $buffer.Delete "new0" -}} {{- range ($buffer.Get "new") -}} {{- $folder := . -}} {{- $buffer.Add "all"   (slice .) -}} {{- range (readDir $folder ) -}} {{- if .IsDir -}} {{- $buffer.Add "new0"  (slice (printf "%s/%s" $folder .Name)) -}} {{- end -}} {{- end -}} {{- end -}}	{{- $buffer.Delete "new" -}} {{- $buffer.Add "new" ($buffer.Get "new0") -}} {{- end -}}

{{- range ($buffer.Get "all") -}}{{- $folder := . -}}{{- range (readDir $folder ) -}} {{- if not .IsDir -}} {{- $ext := index (last 1 (split .Name ".")) 0 -}}{{- if ne $ext "md" -}}
	'.{{- (printf "%s/%s"  (substr $folder $nskip) .Name) -}}',
{{ end }}{{- end -}}{{- end -}}{{- end -}}

{{- /* Generate all files in content folder. */ -}}
{{- $rootFolder := "content" -}}
{{- $nskip := len $rootFolder  -}} {{- $buffer := newScratch -}} {{- $buffer.Add "new" (slice $rootFolder) -}}
{{- range (seq 10) -}} {{- $buffer.Delete "new0" -}} {{- range ($buffer.Get "new") -}} {{- $folder := . -}} {{- $buffer.Add "all"   (slice .) -}} {{- range (readDir $folder ) -}} {{- if .IsDir -}} {{- $buffer.Add "new0"  (slice (printf "%s/%s" $folder .Name)) -}} {{- end -}} {{- end -}} {{- end -}}	{{- $buffer.Delete "new" -}} {{- $buffer.Add "new" ($buffer.Get "new0") -}} {{- end -}}

{{- range ($buffer.Get "all") -}}{{- $folder := . -}}{{- range (readDir $folder ) -}} {{- if not .IsDir -}} {{- $ext := index (last 1 (split .Name ".")) 0 -}}{{- if ne $ext "md" -}}
	'.{{- (printf "%s/%s"  (substr $folder $nskip) .Name) -}}',
{{ end }}{{- end -}}{{- end -}}{{- end -}}

{{- /* Generate all pages. */ -}}
{{- range site.Sections -}}
	'.{{- .Permalink -}}',	
{{ end }}
{{- range site.RegularPages -}}
	'.{{- .Permalink -}}',	
{{ end }}

];

self.addEventListener('install', (ev) => {
  console.log(`install`);
  self.skipWaiting();
  ev.waitUntil(caches.open(cacheName).then((cache) => cache.addAll(precache)));
});

self.addEventListener('activate', (ev) => {
  console.log(`activate`);
  ev.waitUntil(self.clients.claim());
  ev.waitUntil(
    caches.keys().then((keyList) =>
      Promise.all(
        keyList.map((key) => {
          if (key !== cacheName) {
            return caches.delete(key);
          }
        })
      )
    )
  );
});

const fromCache = (request) =>
  caches.open(cacheName).then((cache) => cache.match(request));

const updateCache = (request, response) =>
  caches.open(cacheName).then((cache) => cache.put(request, response));

const fetchAndCache = (ev) =>
  fetch(ev.request)
    .then((response) => {
      if (!response || response.status !== 200 || response.type !== 'basic') {
        return response;
      }
      ev.waitUntil(updateCache(ev.request, response.clone()));
      return response;
    })
    .catch((err) => {
      console.log(err);
    });

self.addEventListener('fetch', (ev) => {
  if (ev.request.method !== 'GET') {
    return;
  }
  
  // check if request is made by chrome extensions or web page
  // if request is made for web page url must contains http.
  if (!(ev.request.url.indexOf('http') === 0)) return; // skip the request. if request is not made with http protocol
  
  ev.respondWith(
    fromCache(ev.request).then((response) => {
      if (response) {
        //console.log(`from cache: ${url.pathname}`);
        ev.waitUntil(fetchAndCache(ev));
        return response;
      }
      //console.log(`from fetch: ${url.pathname}`);
      return fetchAndCache(ev);
    })
  );
});
